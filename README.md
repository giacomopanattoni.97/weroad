# Laravel Project - WeRoad Hiring Test

This project is a Laravel API application that simulates the structure of WeRoad. It includes both public and private endpoints with roles.

## Requirements

- PHP 8.1 or higher
- Composer 2.5.5
- [Laravel 10](https://laravel.com/docs/10.x/)
- [Laravel Sail](https://laravel.com/docs/10.x/sail) (for Docker development environment)
- [Pest](https://pestphp.com/docs/installation) (for running tests)

## Installation

1. Clone the repository:

`git clone https://gitlab.com/giacomopanattoni.97/weroad.git`

2. Install the project dependencies using Composer:

`composer install`

3. Copy the `.env.example` file to a new file called `.env` and configure the database settings:

`cp .env.example .env`

4. Generate an application key:

`php artisan key:generate`


5. Start the Docker development environment with Laravel Sail:

`sail up`

6. Run the database migrations and seeders:

`sail composer dev-reset`

or:

`sail php artisan migrate:fresh && sail php artisan db:seed`

## User Creation

A Laravel Artisan command is available to create users with specific roles. To create a new user, run the following command:

`sail php artisan user:create {name} {email} {password} {--admin}`

## Usage

The project includes several API endpoints for managing travels, tours, and users. Below is a brief summary of the available endpoints:

### Public Endpoints (no authentication required)

- `POST /api/login`: User login.
- `GET /api/travels`: Get a paginated list of public travels.
- `GET /api/travels/{slug}/tours`: Get a paginated list of tours for a specific travel using its slug.

### Private Endpoints (authentication required)

- `POST /api/users`: Create a new user (administrators only).
- `POST /api/travels`: Create a new travel (administrators only).
- `POST /api/tours`: Create a new tour for a specific travel (administrators only).
- `PUT /api/travels/{id}`: Update an existing travel (administrators and editors).

## Tests

The project tests have been written using the Pest PHP library. To run the tests, use the following command:

`sail php ./vendor/bin/pest`

The tests cover basic functionalities for managing users, travels, and tours, including authentication and authorization checks.

## Code Quality

The code has been checked with the following libraries:

- php-cs-fixer: for coding standards and style
- Larastan: for static analysis and type checking

## Documentation

For more detailed documentation on the available endpoints and parameters, please refer to the [API documentation](./weroad_test.postman_collection.json).
