<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CreateUserCommand extends Command
{
    protected $signature = 'user:create {name} {email} {password} {--admin}';

    protected $description = 'Create a new user';

    public function handle()
    {
        $email = $this->argument('email');
        $name = $this->argument('name');
        $password = $this->argument('password');
        $isAdmin = $this->option('admin');

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        if ($isAdmin) {
            $user->assignRole(Role::ROLE_ADMIN);
        } else {
            $user->assignRole(Role::ROLE_EDITOR);
        }

        $user->save();

        $this->info('User created successfully.');
    }
}
