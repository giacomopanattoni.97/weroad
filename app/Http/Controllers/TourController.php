<?php

namespace App\Http\Controllers;

use App\Models\Tour;
use App\Models\Travel;
use Illuminate\Http\Request;

class TourController extends Controller
{
    public function store(Request $request)
    {
        $validatedRequest = $request->validate([
            'travelId' => 'required|exists:travels,id',
            'name' => 'required',
            'startingDate' => 'required|date',
            'endingDate' => 'required|date|after_or_equal:startingDate',
            'price' => 'required|integer',
        ]);

        $tour = Tour::create($validatedRequest);

        return response()->json(['tour' => $tour, 'message' => 'Tour created successfully!'], 201);
    }

    public function index(Request $request, $slug)
    {
        $request->validate([
            'page' => 'integer|min:1',
            'perPage' => 'integer|min:1|max:100',
            'priceFrom' => 'integer|min:0',
            'priceTo' => 'integer|min:0',
            'dateFrom' => 'date|nullable',
            'dateTo' => 'date|nullable|after_or_equal:dateFrom',
            'sort' => 'in:asc,desc',
        ]);

        $travel = Travel::where([
            'slug' => $slug,
            'isPublic' => true
        ])->first();
        if ($travel) {
            $query = $travel->tours();

            if ($request->has('priceFrom')) {
                $query->where('price', '>=', $request->priceFrom * 100);
            }

            if ($request->has('priceTo')) {
                $query->where('price', '<=', $request->priceTo * 100);
            }

            if ($request->has('dateFrom')) {
                $query->where('startingDate', '>=', $request->dateFrom);
            }

            if ($request->has('dateTo')) {
                $query->where('startingDate', '<=', $request->dateTo);
            }

            $query->orderBy('price', $request->input('sort', 'asc') === 'asc' ? 'asc' : 'desc');
            $query->orderBy('startingDate', 'asc');

            $tours = $query->paginate($request->input('perPage', 10));

            return response()->json($tours);
        } else {
            return response()->json(['message' => 'Travel not found'], 404);
        }
    }
}
