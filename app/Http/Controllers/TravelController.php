<?php

namespace App\Http\Controllers;

use App\Models\Travel;
use Closure;
use Illuminate\Http\Request;

class TravelController extends Controller
{
    public function store(Request $request)
    {
        $requestValidated = $request->validate([
            'isPublic' => 'required|boolean',
            'slug' => 'required|unique:travels',
            'name' => 'required|string',
            'description' => 'required|string',
            'numberOfDays' => 'required|integer',
            'moods' => [
                'array',
                function (string $attribute, mixed $value, Closure $fail) {
                    if (
                        !isset($value['nature']) ||
                        !isset($value['relax']) ||
                        !isset($value['history']) ||
                        !isset($value['culture']) ||
                        !isset($value['party'])
                    ) {
                        $fail("The {$attribute} attribute is invalid.");
                    }
                },
            ],
        ]);

        $travel = Travel::create($requestValidated);

        return response()->json(['travel' => $travel, 'message' => 'Travel created successfully!'], 201);
    }

    public function update(Request $request, Travel $travel)
    {
        $validatedRequest = $request->validate([
            'isPublic' => 'boolean',
            'slug' => 'unique:travels',
            'name' => 'string',
            'description' => 'string',
            'numberOfDays' => 'integer',
            'moods' => [
                'array',
                function (string $attribute, mixed $value, Closure $fail) {
                    if (
                        !isset($value['nature']) ||
                        !isset($value['relax']) ||
                        !isset($value['history']) ||
                        !isset($value['culture']) ||
                        !isset($value['party'])
                    ) {
                        $fail("The {$attribute} attribute is invalid.");
                    }
                },
            ],
        ]);

        $travel->update($validatedRequest);

        return response()->json(['travel' => $travel, 'message' => 'Travel updated successfully!']);
    }

    public function index(Request $request)
    {
        $travels = Travel::where('isPublic', true)->paginate(10);

        return response()->json($travels);
    }
}
