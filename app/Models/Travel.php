<?php

namespace App\Models;

use App\Casts\Moods;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Travel
 *
 * @property string $id
 * @property int $isPublic
 * @property string $slug
 * @property string $name
 * @property string $description
 * @property int $numberOfDays
 * @property mixed|null|null $moods
 * @property-read mixed $number_of_nights
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Tour> $tours
 * @property-read int|null $tours_count
 * @method static \Database\Factories\TravelFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Travel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Travel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Travel query()
 * @method static \Illuminate\Database\Eloquent\Builder|Travel whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Travel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Travel whereIsPublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Travel whereMoods($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Travel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Travel whereNumberOfDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Travel whereSlug($value)
 * @mixin \Eloquent
 */
class Travel extends Model
{
    use HasFactory;
    use HasUuids;

    protected $table = "travels";

    public $incrementing = false;
    protected $keyType = 'string';

    public $timestamps = false;

    protected $casts = [
        'moods' => Moods::class,
    ];

    protected $fillable = [
        'isPublic',
        'slug',
        'name',
        'description',
        'numberOfDays',
        'moods',
    ];

    protected $appends = ['numberOfNights'];


    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    public function getNumberOfNightsAttribute()
    {
        return $this->numberOfDays - 1;
    }



    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */
    public function tours()
    {
        return $this->hasMany(Tour::class, 'travelId', 'id');
    }
}
