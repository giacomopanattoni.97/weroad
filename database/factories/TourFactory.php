<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tour>
 */
class TourFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "name" => fake()->title(),
            "startingDate" => fake()->dateTimeBetween('-1 week', '+1 week')->format('Y-m-d'),
            "endingDate" => fake()->dateTimeBetween('+2 week', '+3 week')->format('Y-m-d'),
            "price" => fake()->numberBetween(1000, 4000)
        ];
    }
}
