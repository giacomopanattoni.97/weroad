<?php

namespace Database\Factories;

use App\Models\Travel;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Travel>
 */
class TravelFactory extends Factory
{
    protected $model = Travel::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "slug" => fake()->slug(),
            "name" => fake()->name(),
            "description" => fake()->text(),
            "numberOfDays" => fake()->numberBetween(3, 20),
            "isPublic" => fake()->boolean(80),
            "moods" => [
                "nature" => fake()->numberBetween(1, 100),
                "relax" => fake()->numberBetween(1, 100),
                "history" => fake()->numberBetween(1, 100),
                "culture" => fake()->numberBetween(1, 100),
                "party" => fake()->numberBetween(1, 100),
            ],
        ];
    }
}
