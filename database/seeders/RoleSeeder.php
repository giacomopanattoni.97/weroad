<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create([
            "id" => "baf18948-721e-49f5-aa7f-bed1a5415cb6",
            "name" =>  Role::ROLE_ADMIN
        ]);
        Role::create([
            "id" => "9442703c-dd4f-4e36-9554-a60574c408be",
            "name" => Role::ROLE_EDITOR
        ]);
    }
}
