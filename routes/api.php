<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\TourController;
use App\Http\Controllers\TravelController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Public Routes
|--------------------------------------------------------------------------
*/


Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/travels', [TravelController::class, 'index']);
Route::get('/travels/{slug}/tours', [TourController::class, 'index']);

/*
|--------------------------------------------------------------------------
| Protected Routes
|--------------------------------------------------------------------------
*/

Route::middleware(['auth:sanctum'])->group(function () {
    Route::middleware(["role:admin"])->group(function () {
        Route::post('/users', [UserController::class, 'store']);
        Route::post('/travels', [TravelController::class, 'store']);
        Route::post('/tours', [TourController::class, 'store']);
    });

    Route::middleware(['role:editor'])->group(function () {
        Route::put('/travels/{travel}', [TravelController::class, 'update']);
    });
});
