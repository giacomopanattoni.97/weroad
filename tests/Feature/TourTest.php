<?php

use App\Models\Role;
use App\Models\Tour;
use App\Models\Travel;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

use function Pest\Laravel\{actingAs};

uses(RefreshDatabase::class);

it('Admin can create tours', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_ADMIN);

    $travel = Travel::factory()->create();
    $tour = Tour::factory()->raw();
    $tour['travelId'] = $travel->id;

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/tours', $tour)
        ->assertStatus(201);

    $this->assertDatabaseHas('tours', [
        "travelId" => $tour['travelId'],
        "name" => $tour['name'],
        "startingDate" => $tour['startingDate'],
        "endingDate" => $tour['endingDate'],
        "price" => $tour['price'],
    ]);
});

it('Editor can\t create tours', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_EDITOR);

    $travel = Travel::factory()->create();
    $tour = Tour::factory()->raw();
    $tour['travelId'] = $travel->id;

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/tours', $tour)
        ->assertStatus(403);
});

it('Unauthenticated user can\t create tours', function () {

    $travel = Travel::factory()->create();
    $tour = Tour::factory()->raw();
    $tour['travelId'] = $travel->id;

    $this
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/tours', $tour)
        ->assertStatus(401);
});

it('All users can view tours of a public travel', function () {

    $travel = Travel::factory()->create([
        'isPublic' => true
    ]);

    $tour = Tour::factory()->raw();
    $tour['travelId'] = $travel->id;
    $tour = Tour::create($tour);

    $this
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->get("/api/travels/$travel->slug/tours")
        ->assertStatus(200);
});
