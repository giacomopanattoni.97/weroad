<?php

use App\Models\Role;
use App\Models\Travel;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

use function Pest\Laravel\{actingAs};

uses(RefreshDatabase::class);

it('Admin can create travels', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_ADMIN);

    $newTravel = Travel::factory()->raw();

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/travels', $newTravel)
        ->assertStatus(201);

    $this->assertDatabaseHas('travels', [
        "name" => $newTravel['name'],
        "slug" => $newTravel['slug'],
        "description" => $newTravel['description'],
        "numberOfDays" => $newTravel['numberOfDays'],
    ]);
});

it('Admin can\'t create travels with the same slug', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_ADMIN);

    $newTravel = Travel::factory()->raw();

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/travels', $newTravel)
        ->assertStatus(201);

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/travels', $newTravel)
        ->assertStatus(422);
});

it('Editor can\t create travels', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_EDITOR);

    $newTravel = Travel::factory()->raw();

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/travels', $newTravel)
        ->assertStatus(403);
});

it('Unauthenticated user can\t create travels', function () {
    $newTravel = Travel::factory()->raw();

    $this->withHeaders([
        'Accept' => 'application/json',
    ])->post('/api/travels', $newTravel)
        ->assertStatus(401);
});

it('All users can list public travels', function () {
    $admin = User::factory()->create();
    $admin->assignRole(Role::ROLE_ADMIN);

    $editor = User::factory()->create();
    $editor->assignRole(Role::ROLE_EDITOR);

    /* admin */
    actingAs($admin)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->get('/api/travels')
        ->assertStatus(200);

    /* editor */
    actingAs($editor)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->get('/api/travels')
        ->assertStatus(200);

    /* Unauthenticated user */
    $this
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->get('/api/travels')
        ->assertStatus(200);
});

it('Admin can update a travel', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_ADMIN);

    $newTravel = Travel::factory()->create();
    $newSlug = fake()->slug();

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->put("/api/travels/$newTravel->id", [
            'slug' => $newSlug
        ])
        ->assertStatus(200);

    $this->assertDatabaseHas('travels', [
        "name" => $newTravel['name'],
        "slug" => $newSlug,
        "description" => $newTravel['description'],
        "numberOfDays" => $newTravel['numberOfDays'],
    ]);
});

it('Editor can update a travel', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_EDITOR);

    $newTravel = Travel::factory()->create();
    $newSlug = fake()->slug();

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->put("/api/travels/$newTravel->id", [
            'slug' => $newSlug
        ])
        ->assertStatus(200);

    $this->assertDatabaseHas('travels', [
        "name" => $newTravel['name'],
        "slug" => $newSlug,
        "description" => $newTravel['description'],
        "numberOfDays" => $newTravel['numberOfDays'],
    ]);
});

it('Unauthenticated user can\t update a travel', function () {
    $newTravel = Travel::factory()->create();
    $newSlug = fake()->slug();

    $this
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->put("/api/travels/$newTravel->id", [
            'slug' => $newSlug
        ])
        ->assertStatus(401);
});
