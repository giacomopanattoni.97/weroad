<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

use function Pest\Laravel\{actingAs};

uses(RefreshDatabase::class);

it('admin can create editors', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_ADMIN);

    $newUser = User::factory()->raw();
    $newUser['roles'] = [Role::ROLE_EDITOR];

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/users', $newUser)
        ->assertStatus(201);

    $this->assertDatabaseHas('users', [
        "name" => $newUser['name'],
        "email" => $newUser['email'],
    ]);
});

it('admin can create admin', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_ADMIN);

    $newUser = User::factory()->raw();
    $newUser['roles'] = [Role::ROLE_ADMIN];

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/users', $newUser)
        ->assertStatus(201);

    $this->assertDatabaseHas('users', [
        "name" => $newUser['name'],
        "email" => $newUser['email'],
    ]);
});

it('Editor can\'t create editor', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_EDITOR);

    $newUser = User::factory()->raw();
    $newUser['roles'] = [Role::ROLE_EDITOR];

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/users', $newUser)
        ->assertStatus(403);
});

it('Editor can\'t create admin', function () {
    $user = User::factory()->create();
    $user->assignRole(Role::ROLE_EDITOR);

    $newUser = User::factory()->raw();
    $newUser['roles'] = [Role::ROLE_ADMIN];

    actingAs($user)
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->post('/api/users', $newUser)
        ->assertStatus(403);
});

it('Unauthenticated user can\'t create users', function () {
    $newUser = User::factory()->raw();
    $newUser['roles'] = [Role::ROLE_ADMIN];

    $this
        ->withHeaders([
            'Accept' => 'application/json',
        ])
        ->postJson('/api/users', $newUser)
        ->assertStatus(401);
});
